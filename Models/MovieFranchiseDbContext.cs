﻿using Microsoft.EntityFrameworkCore;
using MovieFranchiseAPI.Models.Domain;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace MovieFranchiseAPI.Models
{
    public class MovieFranchiseDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieFranchiseDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacterSeeds());
            modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFranchiseSeeds());
            modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovieSeeds());

            modelBuilder.Entity<Character>()
                .HasMany(c => c.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                "CharacterMovie",
                r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                je =>
                {
                    je.HasKey("CharacterId", "MovieId");
                    je.HasData(SeedHelper.GetLinkingSeeds());
                });
        }
    }
}
