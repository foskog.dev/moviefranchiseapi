using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MovieFranchiseAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // What this does is essentially run a migration at the host when your run the app.
            // Host must still be specified in connectionString
            var host = CreateHostBuilder(args).Build();

            var service = (IServiceScopeFactory)host.Services.GetService(typeof(IServiceScopeFactory));

            using (var db = service.CreateScope().ServiceProvider.GetService<MovieFranchiseDbContext>())
            {
                db.Database.Migrate();
            }
                
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
