﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieFranchiseAPI.Models;
using MovieFranchiseAPI.Models.Domain;
using MovieFranchiseAPI.Models.DTOs.Character;
using MovieFranchiseAPI.Models.DTOs.Movie;
using MovieFranchiseAPI.Services;

namespace MovieFranchiseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;


        public MoviesController(IMovieService movieService, IMapper mapper)
        {
            _movieService = movieService;
            _mapper = mapper;
        }

        /// <summary>
        /// Post a movie to the database
        /// </summary>
        /// <returns>Movie Json Information</returns>
        /// <response code="201">Successfully created a movie</response>
        /// <response code="400">Missing required fields</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO dtoMovie)
        {
            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);

            domainMovie = await _movieService.AddMovieAsync(domainMovie);

            return CreatedAtAction("GetMovie",
                new { id = domainMovie.MovieId },
                _mapper.Map<MovieReadDTO>(domainMovie));
        }

        /// <summary>
        /// Gets all the movies in the database
        /// </summary>
        /// <returns>Movie Json Information</returns>
        /// <response code="200">Successfully returns all movies</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetAllMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());
        }

        /// <summary>
        /// Gets Specific movie from the database
        /// </summary>
        /// <returns>Movie Json Information</returns>
        /// <response code="200">Successfully returns a movie</response>
        /// <response code="404">No movie found for id</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            Movie movie = await _movieService.GetMovieByIdAsync(id);

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Gets Characters from movie by id
        /// </summary>
        /// <returns>Movie Json Information</returns>
        /// <response code="200">Successfully returns a list of characters</response>
        /// <response code="404">No movie found for id</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInMovieById(int id)
        {
             if (!_movieService.MovieExists(id))
             {
                return NotFound();
             }

            return _mapper.Map<List<CharacterReadDTO>>(await _movieService.GetCharactersInMovieByIdAsync(id));
        }

        /// <summary>
        /// Update movie in database
        /// </summary>
        /// <returns>Nothing</returns>
        /// <response code="200">Successfully Updated movie</response>
        /// <response code="400">Invalid movie id</response>
        /// <response code="404">No movie found for id</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateMovie(int id, MovieUpdateDTO dtoMovie)
        {
            if (id != dtoMovie.MovieId)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            Movie domainMovie = _mapper.Map<Movie>(dtoMovie);
            await _movieService.UpdateMovieAsync(domainMovie);

            return NoContent();
        }

        /// <summary>
        /// Update characters in Movie in database
        /// </summary>
        /// <returns>Nothing</returns>
        /// <response code="200">Successfully Updated movie</response>
        /// <response code="400">Invalid character ids</response>
        /// <response code="404">No movie found for id</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateCharactersInMovie(int id, List<int> characterIds)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            try
            {
                await _movieService.UpdateCharactersInMovieAsync(id, characterIds);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid CharacterIds");
            }
            return NoContent();
        }

        /// <summary>
        /// Delete movie from database
        /// </summary>
        /// <returns>No content</returns>
        /// <response code="204">Successfully deleted movie</response>
        /// <response code="404">No movie found for id</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);

            return NoContent();
        }
    }
}
