﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieFranchiseAPI.Models;
using MovieFranchiseAPI.Models.Domain;
using MovieFranchiseAPI.Models.DTOs.Character;
using MovieFranchiseAPI.Models.DTOs.Franchise;
using MovieFranchiseAPI.Models.DTOs.Movie;
using MovieFranchiseAPI.Services;

namespace MovieFranchiseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchisesController(IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _mapper = mapper;
        }

        /// <summary>
        /// Post a franchise to the database
        /// </summary>
        /// <returns>Franchise Json Information</returns>
        /// <response code="201">Successfully created a franchise</response>
        /// <response code="400">Missing required fields</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO dtoFranchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);

            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise",
                new { id = domainFranchise.FranchiseId },
                _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Gets all the franchises in the database
        /// </summary>
        /// <returns>Franchise Json Information</returns>
        /// <response code="200">Successfully returns all franchises</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchise()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }

        /// <summary>
        /// Gets Specific franchise from the database
        /// </summary>
        /// <returns>Franchise Json Information</returns>
        /// <response code="200">Successfully returns a franchise</response>
        /// <response code="404">No franchise found for id</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(await _franchiseService.GetFranchiseByIdAsync(id));
        }

        /// <summary>
        /// Gets Movies in franchise by id
        /// </summary>
        /// <returns>Movie Json Information</returns>
        /// <response code="200">Successfully returns a list of movies</response>
        /// <response code="404">No movie found for id</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchiseById(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            return _mapper.Map<List<MovieReadDTO>>(await _franchiseService.GetMoviesInFranchiseByIdAsync(id));
        }

        /// <summary>
        /// Gets Characters from franchise by id
        /// </summary>
        /// <returns>franchise Json Information</returns>
        /// <response code="200">Successfully returns a list of charcters</response>
        /// <response code="404">No franchise found for id</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}/Characters")]
        public async Task<IEnumerable<CharacterReadDTO>> GetCharactersInFranchiseById(int id)
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _franchiseService.GetCharactersInFranchiseByIdAsync(id));
        }

        /// <summary>
        /// Update franchise in database
        /// </summary>
        /// <returns>Nothing</returns>
        /// <response code="200">Successfully Updated franchise</response>
        /// <response code="400">Invalid franchise id</response>
        /// <response code="404">No franchise found for id</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateFranchise(int id, FranchiseUpdateDTO dtoFranchise)
        {
            if (id != dtoFranchise.FranchiseId)
            {
                return BadRequest();
            }

            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            await _franchiseService.UpdateFranchiseAsync(domainFranchise);

            return NoContent();
        }

        /// <summary>
        /// Update movies in franchise in database
        /// </summary>
        /// <returns>Nothing</returns>
        /// <response code="200">Successfully Updated franchise</response>
        /// <response code="400">Invalid franchise ids</response>
        /// <response code="404">No franchise found for id</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMoviesInFranchise(int id, List<int> movieIds)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.UpdateMoviesInFranchiseAsync(id, movieIds);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid MovieId");
            }
            return NoContent();
        }

        /// <summary>
        /// Delete franchise from database
        /// </summary>
        /// <returns>No content</returns>
        /// <response code="204">Successfully deleted franchise</response>
        /// <response code="404">No franchise found for id</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            await _franchiseService.DeleteFranchiseAsync(id);

            return NoContent();
        }
    }
}
