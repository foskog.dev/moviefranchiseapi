﻿using System.ComponentModel.DataAnnotations;

namespace MovieFranchiseAPI.Models.DTOs.Character
{
    public class CharacterUpdateDTO
    {
        public int CharacterId { get; set; }
        [Required, MaxLength(100)]
        public string FullName { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [MaxLength(10)]
        public string Gender { get; set; }
        public string Picture { get; set; }
    }
}
