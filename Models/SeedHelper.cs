﻿using MovieFranchiseAPI.Models.Domain;
using System.Collections.Generic;

namespace MovieFranchiseAPI.Models
{
    public class SeedHelper
    {
        public static IEnumerable<Character> GetCharacterSeeds()
        {
            IEnumerable<Character> seedCharacters = new List<Character>
            {
                new Character
                {
                    CharacterId = 1,
                    FullName = "Tony Stark",
                    Alias = "Iron Man",
                    Gender = "Male",
                    Picture = "http://linktopicture.com/picture",
                },
                new Character
                {
                    CharacterId = 2,
                    FullName = "Bruce Banner",
                    Alias = "The Hulk",
                    Gender = "Male",
                    Picture = "http://linktopicture.com/picture",
                },
                new Character
                {
                    CharacterId = 3,
                    FullName = "Thor",
                    Alias = "Thor",
                    Gender = "Male",
                    Picture = "http://linktopicture.com/picture",
                },
                new Character
                {
                    CharacterId = 4,
                    FullName = "Captain America",
                    Alias = "Steve Rogers",
                    Gender = "Male",
                    Picture = "http://linktopicture.com/picture",
                },
                new Character
                {
                    CharacterId = 5,
                    FullName = "Luke Skywalker",
                    Alias = "Luke Skywalker",
                    Gender = "Male",
                    Picture = "http://linktopicture.com/picture",
                },
                new Character
                {
                    CharacterId = 6,
                    FullName = "Darth Vader",
                    Alias = "Darth Vader",
                    Gender = "Male",
                    Picture = "http://linktopicture.com/picture",
                }
            };
            return seedCharacters;
        }

        public static IEnumerable<Franchise> GetFranchiseSeeds()
        {
            IEnumerable<Franchise> seedFranchises = new List<Franchise>
            {
                new Franchise
                {
                    FranchiseId = 1,
                    Name = "Marvel Cinematic Universe",
                    Description = "Big superhero universe",
                },
                new Franchise
                {
                    FranchiseId = 2,
                    Name = "Star wars",
                    Description = "Space Opera set in a galaxy far far away",
                }
            };
            return seedFranchises;
        }

        public static IEnumerable<Movie> GetMovieSeeds()
        {
            IEnumerable<Movie> seedMovies = new List<Movie>
            {
                new Movie
                {
                    MovieId = 1,
                    Title = "Iron Man",
                    Genre = "Action Adventure",
                    ReleaseYear = 2008,
                    Director = "Some Guy",
                    Picture = "http://linktopicture.com/picture",
                    Trailer = "http://linktotrailer.com/trailer",
                    FranchiseId = 1
                },
                new Movie
                {
                    MovieId = 2,
                    Title = "The Hulk",
                    Genre = "Action Adventure",
                    ReleaseYear = 2010,
                    Director = "Some Guy",
                    Picture = "http://linktopicture.com/picture",
                    Trailer = "http://linktotrailer.com/trailer",
                    FranchiseId = 1
                },
                new Movie
                {
                    MovieId = 3,
                    Title = "Thor",
                    Genre = "Action Adventure",
                    ReleaseYear = 2010,
                    Director = "Some Guy",
                    Picture = "http://linktopicture.com/picture",
                    Trailer = "http://linktotrailer.com/trailer",
                    FranchiseId = 1
                },
                new Movie
                {
                    MovieId = 4,
                    Title = "Captain America",
                    Genre = "Action Adventure",
                    ReleaseYear = 2007,
                    Director = "Some Guy",
                    Picture = "http://linktopicture.com/picture",
                    Trailer = "http://linktotrailer.com/trailer",
                    FranchiseId = 1
                },
                new Movie
                {
                    MovieId = 5,
                    Title = "The Avengers",
                    Genre = "Action Adventure",
                    ReleaseYear = 2011,
                    Director = "Some Guy",
                    Picture = "http://linktopicture.com/picture",
                    Trailer = "http://linktotrailer.com/trailer",
                    FranchiseId = 1
                },
                new Movie
                {
                    MovieId = 6,
                    Title = "Star Wars - A New Hope",
                    Genre = "SciFi",
                    ReleaseYear = 1977,
                    Director = "George Lucas",
                    Picture = "http://linktopicture.com/picture",
                    Trailer = "http://linktotrailer.com/trailer",
                    FranchiseId = 2
                },
                new Movie
                {
                    MovieId = 7,
                    Title = "Star Wars - The empire Strikes back",
                    Genre = "SciFi",
                    ReleaseYear = 1980,
                    Director = "George Lucas",
                    Picture = "http://linktopicture.com/picture",
                    Trailer = "http://linktotrailer.com/trailer",
                    FranchiseId = 2
                },
                new Movie
                {
                    MovieId = 8,
                    Title = "Star Wars - Return of the jedi",
                    Genre = "SciFi",
                    ReleaseYear = 1983,
                    Director = "George Lucas",
                    Picture = "http://linktopicture.com/picture",
                    Trailer = "http://linktotrailer.com/trailer",
                    FranchiseId = 2
                }
            };
            return seedMovies;
        }

        public static IEnumerable<object> GetLinkingSeeds()
        {
            IEnumerable<object> seedLinkingTable = new List<object>
            {
                // Marvel seeds
                new { CharacterId = 1, MovieId = 1 },
                new { CharacterId = 2, MovieId = 2 },
                new { CharacterId = 3, MovieId = 3 },
                new { CharacterId = 4, MovieId = 4 },
                new { CharacterId = 1, MovieId = 5 },
                new { CharacterId = 2, MovieId = 5 },
                new { CharacterId = 3, MovieId = 5 },
                new { CharacterId = 4, MovieId = 5 },

                // Star wars seeds
                new { CharacterId = 5, MovieId = 6 },
                new { CharacterId = 6, MovieId = 6 },
                new { CharacterId = 5, MovieId = 7 },
                new { CharacterId = 6, MovieId = 7 },
                new { CharacterId = 5, MovieId = 8 },
                new { CharacterId = 6, MovieId = 8 },
            };
            return seedLinkingTable;
        }
    }
}
