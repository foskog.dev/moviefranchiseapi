﻿using MovieFranchiseAPI.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Services
{
    public interface IFranchiseService
    {
        // Create
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);

        // Read
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();
        public Task<Franchise> GetFranchiseByIdAsync(int id);
        public Task<IEnumerable<Movie>> GetMoviesInFranchiseByIdAsync(int id);
        public Task<IEnumerable<Character>> GetCharactersInFranchiseByIdAsync(int id);

        // Update
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task UpdateMoviesInFranchiseAsync(int id, List<int> movieIds);

        // Delete
        public Task DeleteFranchiseAsync(int id);

        public bool FranchiseExists(int id);
    }
}
