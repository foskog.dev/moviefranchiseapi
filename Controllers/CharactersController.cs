﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieFranchiseAPI.Models;
using MovieFranchiseAPI.Models.Domain;
using MovieFranchiseAPI.Models.DTOs.Character;
using MovieFranchiseAPI.Services;

namespace MovieFranchiseAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;
        
        public CharactersController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }

        /// <summary>
        /// Post a character to the database
        /// </summary>
        /// <returns>character Json Information</returns>
        /// <response code="201">Successfully created a movie</response>
        /// <response code="400">Missing required fields</response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(CharacterCreateDTO dtoCharacter)
        {
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);

            domainCharacter = await _characterService.AddCharacterAsync(domainCharacter);

            return CreatedAtAction("GetCharacter",
                new { id = domainCharacter.CharacterId },
                _mapper.Map<CharacterReadDTO>(domainCharacter));
        }

        /// <summary>
        /// Gets all the characters in the database
        /// </summary>
        /// <returns>Character Json Information</returns>
        /// <response code="200">Successfully all characters</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharacters()
        {
           return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetAllCharactersAsync());           
        }

        /// <summary>
        /// Gets Specific character from the database
        /// </summary>
        /// <returns>Character Json Information</returns>
        /// <response code="200">Successfully returns a character</response>
        /// <response code="404">No charcter found for id</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            Character character = await _characterService.GetCharacterByIdAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        /// <summary>
        /// Update character in database
        /// </summary>
        /// <returns>Nothing</returns>
        /// <response code="200">Successfully Updated character</response>
        /// <response code="400">Invalid character id</response>
        /// <response code="404">No character found for id</response>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateCharacter(int id, CharacterUpdateDTO dtoCharacter)
        {
            // How are these two not the same?
            // I mean the second one checks the id to make sure a matching object exists.
            if (id != dtoCharacter.CharacterId)
            {
                return BadRequest();
            }

            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            await _characterService.UpdateCharacterAsync(domainCharacter);

            return NoContent();
        }

        /// <summary>
        /// Delete character from database
        /// </summary>
        /// <returns>No content</returns>
        /// <response code="204">Successfully deleted character</response>
        /// <response code="404">No character found for id</response>
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            await _characterService.DeleteCharacterAsync(id);

            return NoContent();
        }
    }
}
