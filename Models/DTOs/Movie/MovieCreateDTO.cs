﻿using System.ComponentModel.DataAnnotations;

namespace MovieFranchiseAPI.Models.DTOs.Movie
{
    public class MovieCreateDTO
    {
        [Required, MaxLength(100)]
        public string Title { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
    }
}
