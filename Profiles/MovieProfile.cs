﻿using AutoMapper;
using MovieFranchiseAPI.Models.Domain;
using MovieFranchiseAPI.Models.DTOs.Movie;
using System.Linq;

namespace MovieFranchiseAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                // Turning related characters into arrays.
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters
                .Select(c => c.CharacterId)
                .ToArray()));
            CreateMap<MovieCreateDTO, Movie>();
            CreateMap<MovieUpdateDTO, Movie>();
        }
    }
}
