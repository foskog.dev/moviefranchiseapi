﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieFranchiseAPI.Models.Domain
{
    public class Character
    {
        // PK
        public int CharacterId { get; set; }
        // Fields
        [Required, MaxLength(100)]
        public string FullName { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [MaxLength(10)]
        public string Gender { get; set; }
        public string Picture { get; set; }
        // Composite key for many to many relationship with movie.
        public ICollection<Movie> Movies { get; set; }
    }
}
