﻿using Microsoft.EntityFrameworkCore;
using MovieFranchiseAPI.Models;
using MovieFranchiseAPI.Models.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Services
{
    /// <summary>
    /// Service class for the MovieController.
    /// Holds all functionality to interact with the movie table in the database through the dbContext.
    /// </summary>
    public class MovieService : IMovieService
    {
        private readonly MovieFranchiseDbContext _context;

        public MovieService(MovieFranchiseDbContext context)
        {
            _context = context;
        }

        // Create Routes

        /// <summary>
        /// Adds a single movie object to the database.
        /// </summary>
        /// <param name="franchise">franchise object</param>
        /// <returns>The franchise object that is added to the database.</returns>
        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        // Read Routes

        /// <summary>
        /// Gets every movie object in the database.
        /// </summary>
        /// <returns>Every movie in the database in an IEnumarable</returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies.Include(m => m.Characters).ToListAsync();
        }

        /// <summary>
        /// Gets a specific movie object from the database by their Id.
        /// </summary>
        /// <param name="id">Id of the movie</param>
        /// <returns>A single movie object matching the id</returns>
        public async Task<Movie> GetMovieByIdAsync(int id)
        {
            return await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.MovieId == id);
        }

        /// <summary>
        /// Gets a list of all the chracters in a movie by the movie's id.
        /// </summary>
        /// <param name="id">id of the movies</param>
        /// <returns>IEnumerable of characters</returns>
        public async Task<IEnumerable<Character>> GetCharactersInMovieByIdAsync(int id)
        {
            return await _context.Characters
                .Include(c => c.Movies)
                .Where(c => c.Movies
                .Any(m => m.MovieId == id))
                .ToListAsync();
        }

        // Update Routes

        /// <summary>
        /// Update a movie object by passing in a new one with the values you want to replace.
        /// </summary>
        /// <param name="movie">The movie object for updating the existing entry.</param>
        /// <returns>Nothing</returns>
        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Populates the relationship between a movie and a list of characters that should belong to that movie.
        /// </summary>
        /// <param name="id">Id of the movie</param>
        /// <param name="characters">List of ids belonging to the characters that belong to the movie</param>
        /// <returns>Nothing</returns>
        /// <exception cref="KeyNotFoundException">id does not exist in franchise.</exception>
        public async Task UpdateCharactersInMovieAsync(int id, List<int> characters)
        {
            Movie movieToUpdateChars = await _context.Movies
                .Include(m => m.Characters)
                .Where(m => m.MovieId == id)
                .FirstAsync();

            List<Character> chars = new();
            foreach (int characterId in characters)
            {
                Character character = await _context.Characters.FindAsync(characterId);
                if (character == null)
                    throw new KeyNotFoundException();
                chars.Add(character);
            }

            movieToUpdateChars.Characters = chars;
            await _context.SaveChangesAsync();
        }

        // Delete Routes

        /// <summary>
        /// Delete a movie from the database in the passed in id.
        /// </summary>
        /// <param name="id">the id of the movie you want to delete.</param>
        /// <returns>Nothing</returns>
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// helper method to check wether a movie already exists.
        /// </summary>
        /// <param name="id">id of the movie you want to check</param>
        /// <returns>true for existing, false for non-existing.</returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }
    }
}
