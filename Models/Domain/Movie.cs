﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieFranchiseAPI.Models.Domain
{
    public class Movie
    {
        // PK
        public int MovieId { get; set; }
        // Fields
        [Required, MaxLength(100)]
        public string Title { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        // Foreign key for many to one relationship with franchise
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        // Composite key for many to many relationship with character
        public ICollection<Character> Characters { get; set; }
    }
}
