﻿using AutoMapper;
using MovieFranchiseAPI.Models.Domain;
using MovieFranchiseAPI.Models.DTOs.Character;
using System.Linq;

namespace MovieFranchiseAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                // Turning related movies into arrays.
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies
                .Select(m => m.MovieId)
                .ToArray()));
            CreateMap<CharacterCreateDTO, Character>();
            CreateMap<CharacterUpdateDTO, Character>();
        }
    }
}
