﻿using AutoMapper;
using MovieFranchiseAPI.Models.Domain;
using MovieFranchiseAPI.Models.DTOs.Franchise;
using System.Linq;

namespace MovieFranchiseAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                // turning related movies into arrays.
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies
                .Select(f => f.MovieId)
                .ToArray()));
            CreateMap<FranchiseCreateDTO, Franchise>();
            CreateMap<FranchiseUpdateDTO, Franchise>();
        }

    }
}
