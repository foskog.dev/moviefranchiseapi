﻿using MovieFranchiseAPI.Models.Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieFranchiseAPI.Models.DTOs.Franchise
{
    public class FranchiseReadDTO
    {
        public int FranchiseId { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }
        public List<int> Movies { get; set; }
    }
}
