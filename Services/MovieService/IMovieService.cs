﻿using MovieFranchiseAPI.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Services
{
    public interface IMovieService
    {
        // Create Routes
        public Task<Movie> AddMovieAsync(Movie movie);

        // Read Routes
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();
        public Task<Movie> GetMovieByIdAsync(int id);
        public Task<IEnumerable<Character>> GetCharactersInMovieByIdAsync(int id);

        // Update Routes
        public Task UpdateMovieAsync(Movie movie);
        public Task UpdateCharactersInMovieAsync(int id, List<int> characters);

        // Delete Routes
        public Task DeleteMovieAsync(int id);

        public bool MovieExists(int id);
    }
}
