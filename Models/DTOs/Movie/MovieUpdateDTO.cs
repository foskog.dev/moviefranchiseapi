﻿using System.ComponentModel.DataAnnotations;

namespace MovieFranchiseAPI.Models.DTOs.Movie
{
    public class MovieUpdateDTO
    {
        public int MovieId { get; set; }
        [Required, MaxLength(100)]
        public string Title { get; set; }
        [MaxLength(100)]
        public string Genre { get; set; }
        [Required]
        public int ReleaseYear { get; set; }
        [MaxLength(50)]
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        //Omitting franchise id here as it is set with the update function in the api.
    }
}
