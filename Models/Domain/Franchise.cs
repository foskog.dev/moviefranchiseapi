﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieFranchiseAPI.Models.Domain
{
    public class Franchise
    {
        // PK
        public int FranchiseId { get; set; }
        // Fields
        [MaxLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }

        // Composite key for many to many relationship with character.
        public ICollection<Movie> Movies { get; set; }
    }
}
