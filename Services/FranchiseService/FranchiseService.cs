﻿using Microsoft.EntityFrameworkCore;
using MovieFranchiseAPI.Models;
using MovieFranchiseAPI.Models.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Services
{
    /// <summary>
    /// Service class for the FranchiseController.
    /// Holds all functionality to interact with the franchise table in the database through the dbContext.
    /// </summary>
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieFranchiseDbContext _context;

        public FranchiseService(MovieFranchiseDbContext context)
        {
            _context = context;
        }

        // Create Routes

        /// <summary>
        /// Adds a single franchise object to the database.
        /// </summary>
        /// <param name="franchise">franchise object</param>
        /// <returns>The franchise object that is added to the database.</returns>
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        // Read Routes

        /// <summary>
        /// Gets every franchise object in the database.
        /// </summary>
        /// <returns>Every franchise in the database in an IEnumarable</returns>
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises.Include(f => f.Movies).ToListAsync();
        }

        /// <summary>
        /// Gets a specific franchise object from the database by their Id.
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <returns>A single franchise object matching the id.</returns>
        public async Task<Franchise> GetFranchiseByIdAsync(int id)
        {
            return await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.FranchiseId == id);
        }

        /// <summary>
        /// Gets a list of all the movies in a franchise by the franchise's id.
        /// </summary>
        /// <param name="id">id of the franchise</param>
        /// <returns>IEnumerable of movies</returns>
        public async Task<IEnumerable<Movie>> GetMoviesInFranchiseByIdAsync(int id)
        {
            return await _context.Movies
                .Where(m => m.FranchiseId == id)
                .Include(m => m.Characters)
                .ToListAsync();
        }

        /// <summary>
        /// Gets all the characters in a franchise across all the movies connected to that franchise.
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <returns>IEnumerable of all characters in franchise.</returns>
        public async Task<IEnumerable<Character>> GetCharactersInFranchiseByIdAsync(int id)
        {
            var moviesInFranchise = await _context.Movies.Where(m => m.FranchiseId == id).ToListAsync();

            List<Character> characters = new List<Character>();

            foreach (var movie in moviesInFranchise)
            {
                var character = await _context.Characters
                    .Include(c => c.Movies)
                    .Where(c => c.Movies
                    .Any(m => m.MovieId == movie.MovieId))
                    .FirstOrDefaultAsync();
                characters.Add(character);
            }

            return characters.Distinct();
        }

        // Update Routes

        /// <summary>
        /// Update a franchise object by passing in a new one with the values you want to replace.
        /// </summary>
        /// <param name="franchise">The franchise object for updating the existing entry.</param>
        /// <returns>Nothing</returns>
        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Populates the relationship between a franchise and a list of movies that should belong to that franchise.
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <param name="movieIds">List of ids belonging to the movies that belong to the franchise</param>
        /// <returns>Nothing</returns>
        /// <exception cref="KeyNotFoundException">id does not exist in franchise.</exception>
        public async Task UpdateMoviesInFranchiseAsync(int id, List<int> movieIds)
        {
            Franchise franchiseToUpdateMovies = await _context.Franchises.FindAsync(id);
            List<Movie> movies = new List<Movie>();
            foreach (int movieId in movieIds)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                    throw new KeyNotFoundException();
                movies.Add(movie);
            }
            franchiseToUpdateMovies.Movies = movies;
            await _context.SaveChangesAsync();
        }

        // Delete Routes

        /// <summary>
        /// Delete a franchise from the database in the passed in id.
        /// </summary>
        /// <param name="id">the id of the franchise you want to delete.</param>
        /// <returns>Nothing</returns>
        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// helper method to check wether a franchise already exists.
        /// </summary>
        /// <param name="id">id of the franchise you want to check</param>
        /// <returns>true for existing, false for non-existing.</returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }
    }
}
