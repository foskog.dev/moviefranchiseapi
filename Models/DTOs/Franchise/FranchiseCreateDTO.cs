﻿using System.ComponentModel.DataAnnotations;

namespace MovieFranchiseAPI.Models.DTOs.Franchise
{
    public class FranchiseCreateDTO
    {
        [MaxLength(100)]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
