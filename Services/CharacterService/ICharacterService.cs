﻿using MovieFranchiseAPI.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Services
{
    public interface ICharacterService
    {
        // Create
        public Task<Character> AddCharacterAsync(Character character);

        // Read
        public Task<IEnumerable<Character>> GetAllCharactersAsync();
        public Task<Character> GetCharacterByIdAsync(int id);

        // Update
        public Task UpdateCharacterAsync(Character character);

        // Delete
        public Task DeleteCharacterAsync(int id);

        public bool CharacterExists(int id);
    }
}
