﻿using Microsoft.EntityFrameworkCore;
using MovieFranchiseAPI.Models;
using MovieFranchiseAPI.Models.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieFranchiseAPI.Services
{
    /// <summary>
    /// Service class for the CharacterController.
    /// Holds all functionality to interact with the character table in the database through the dbContext.
    /// </summary>
    public class CharacterService : ICharacterService
    {
        private readonly MovieFranchiseDbContext _context;

        public CharacterService(MovieFranchiseDbContext context)
        {
            _context = context;
        }

        // Create Routes

        /// <summary>
        /// Adds a single character object to the database.
        /// </summary>
        /// <param name="character">Character object</param>
        /// <returns>The character object that is added to the database.</returns>
        public async Task<Character> AddCharacterAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        // Read Routes

        /// <summary>
        /// Gets every character object on the database.
        /// </summary>
        /// <returns>Every character in the database in an IEnumarable</returns>
        public async Task<IEnumerable<Character>> GetAllCharactersAsync()
        {
            return await _context.Characters.Include(c => c.Movies).ToListAsync();
        }

        /// <summary>
        /// Gets a specific character object from the database by their Id.
        /// </summary>
        /// <param name="id">Id of the character</param>
        /// <returns>A single character object matching the id.</returns>
        public async Task<Character> GetCharacterByIdAsync(int id)
        {
            return await _context.Characters.Include(c => c.Movies).FirstOrDefaultAsync(c => c.CharacterId == id);
        }

        // Update Routes

        /// <summary>
        /// Update a character object by passing in a new one with the values you want to replace.
        /// </summary>
        /// <param name="character">The character object for updating the existing entry.</param>
        /// <returns>Nothing</returns>
        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        // Delete Routes
        /// <summary>
        /// Delete a character from the database in the passed in id.
        /// </summary>
        /// <param name="id">the id of the character you want to delete.</param>
        /// <returns>Nothing</returns>
        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }
        /// <summary>
        /// helper method to check wether a character already exists.
        /// </summary>
        /// <param name="id">id of the character you want to check</param>
        /// <returns>true for existing, false for non-existing.</returns>
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.CharacterId == id);
        }
    }
}
